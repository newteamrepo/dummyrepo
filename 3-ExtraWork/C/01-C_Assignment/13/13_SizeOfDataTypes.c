/*
13. Print the sizes of basic Data types (Repeat the excercise with unsigned)
	 i. char
	ii.	short
   iii.	int
	iv. float
	 v. double
	vi. long

	Hint:
	printf("%d \n", sizeof( <type-name> ));
*/
//--------------------------------------------------------------------------------------------
//code
//--------------------------------------------------------------------------------------------
#include <stdio.h>

int main(void)
{
	printf("\nSize of Basic Data types (in bytes)\n");
	printf("Signed\t\t\tUnsigned\n\n");
	printf("Size of char = %d\tSize of char = %d\n", sizeof(char), sizeof(unsigned char));
	printf("Size of short = %d\tSize of short = %d\n", sizeof(short), sizeof(unsigned short));
	printf("Size of int = %d\t\tSize of int = %d\n", sizeof(int), sizeof(unsigned int));
	printf("Size of float = %d\tSize of float = NA\n", sizeof(float));
	printf("Size of long = %d\tSize of long = %d\n", sizeof(long), sizeof(unsigned long));
	printf("Size of double = %d\tSize of double = NA\n", sizeof(double));

}