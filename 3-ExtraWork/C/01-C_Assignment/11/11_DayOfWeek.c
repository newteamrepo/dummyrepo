/*
11. Write a program to accept a number(0 to 7) and display Day of the Week.
Note : Use switch case statements and enum.
*/
//-----------------------------------------------------------------------------------------
//code
//-----------------------------------------------------------------------------------------
#include<stdio.h>
enum DaysOfWeek { Monday=1, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };
int main(void)
{
	int a;

	printf("Enter the number between 1-7\n");
	scanf("%d", &a);

	switch (a)
	{
	case 1:
		printf("Day : Monday =");
		printf("%d\n", Monday);
		break;

	case 2:
		printf("Day : Tuesday =");
		printf("%d\n", Tuesday);
		break;

	case 3:
		printf("Day : Wednesday =");
		printf("%d\n", Wednesday);
		break;

	case 4:
		printf("Day : Thursday =");
		printf("%d\n", Thursday);
		break;

	case 5:
		printf("Day : Friday =");
		printf("%d\n", Friday);
		break;

	case 6:
		printf("Day : Saturday =");
		printf("%d\n", Saturday);
		break;

	case 7:
		printf(" Favourite Day : Sunday =");
		printf("%d\n", Sunday);
		break;

	default:
		printf("Enter number within specified range only\n");
		break;
	}
}