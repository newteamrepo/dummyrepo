/*
5. 	Write a Program to Accept a number and print the number in character,decimal,octal and hex formats.
*/
//------------------------------------------------------------------------------------------------------
//code
//------------------------------------------------------------------------------------------------------
#include<stdio.h>

int main(void)
{
	int Number;
	printf("Enter any number\n");
	scanf("%d", &Number);
	printf("Your number in character format: %c\n", Number);
	printf("Your number in decimal format: %f\n", (float)Number);
	printf("your number in octal format: %o\n", Number);
	printf("Your number in hexa-decimal format: %x", Number);
}