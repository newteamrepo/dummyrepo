/*
15. Print the limits of datatypes.
	Though all details of a datatypes are defined in "limits.h" header file calculating it is a good exercise.
	All you have to do is extend your prevoius exercises
	1. Calculate size of the datatype
	2. Multiply the size by 8 (why 8?, will be explained in Fundamentals). And use it for power.
	3. min value can be calculated as -(1<<(power-1))
	4. max value can be calculated as (1<<(power-1)) - 1
	5. make sure your text is consistantly aligned.

	Repeat the excercise with unsigned  keyword like "unsigned int", "unsigned short" and so on. Observe the results.

	Note:
	Data Type			Format Specifier
	char						%c
	short						%d
	int							%d
	long						%l
	float						%f
	double						%g

	In case of floating point numbers use %E and %e, and Observe Results
	Get the output in below tabular formats
	-----------------------------------------------------------------------------------
	Data Ttype				Size 		Format Specifier					Range
	-----------------------------------------------------------------------------------
	char					1					%c						-128 to +127
	unsigned char 			1					%c						   0 to 255
	.
	.
	.
	.
	.
	-----------------------------------------------------------------------------------
	Note : For formating the Table use "Escape Sequences" and not Spaces in printf
*/
//--------------------------------------------------------------------------------------
//code
//--------------------------------------------------------------------------------------
#include<stdio.h>
#include<math.h>

int main(void)
{
	int size = 0, i;
	int PowerOfChar	= 8 * sizeof(char);
	PowerOfChar = PowerOfChar - 1;
	int PowerOfUnsignedChar	= 8 * sizeof(unsigned char);
	int PowerOfInt = 8 * sizeof(int);
	PowerOfInt = PowerOfInt - 1;
	int PowerOfUnsignedInt = 8 * sizeof(unsigned int);
	int PowerOfShort = 8 * sizeof(short);
	PowerOfShort = PowerOfShort - 1;
	int PowerOfUnsignedShort = 8 * sizeof(unsigned short);
	int PowerOfLong = 8 * sizeof(long);
	PowerOfLong = PowerOfLong - 1;
	int PowerOfUnsignedLong = 8 * sizeof(unsigned long);
	int PowerOfFloat = 8 * sizeof(float);
	PowerOfFloat = PowerOfFloat - 1;
	int PowerOfdouble = 8 * sizeof(double);
	PowerOfdouble = PowerOfdouble - 1;

	printf("-------------------------------------------------------------------------------------\n");
	printf("Data Types\t\tSize\tFormat Specifier\t\tRange\n");
	printf("-------------------------------------------------------------------------------------\n");
	printf("\nchar\t\t\t%d\t\t%%c\t\t\t%d to %d",sizeof(char),-(1<<PowerOfChar), ((1<<PowerOfChar)-1));
	printf("\nunsigned char\t\t%d\t\t%%c\t\t\t0 to %d",sizeof(unsigned char),((1<<PowerOfUnsignedChar)-1));
	
	printf("\nshort\t\t\t%d\t\t%%d\t\t\t%d to %d", sizeof(short), -(1 << PowerOfShort), ((1 << PowerOfShort) - 1));
	printf("\nunsigned short\t\t%d\t\t%%d\t\t\t0 to %d", sizeof(unsigned short), ((1 << PowerOfUnsignedShort) - 1));
	
	printf("\nint\t\t\t%d\t\t%%d\t\t\t%d to %d", sizeof(int), -(1 << PowerOfInt), ((1 << PowerOfInt) - 1));
	printf("\nunsigned int\t\t%d\t\t%%u\t\t\t0 to %u", sizeof(unsigned int), ((unsigned int)pow(2, PowerOfUnsignedInt) - 1));

	printf("\nlong\t\t\t%d\t\t%%l\t\t\t%d to %d", sizeof(long), -(1 << PowerOfLong), ((1 << PowerOfLong) - 1));
	printf("\nunsigned long\t\t%d\t\t%%lu\t\t\t0 to %lu", sizeof(unsigned long), ((unsigned long)pow(2, PowerOfUnsignedLong) - 1));

	printf("\nfloat\t\t\t%d\t\t%%f\t\t\t%f to %f", sizeof(float), -(1 << PowerOfFloat), ((1 << PowerOfFloat) - 1));
	printf("\ndouble\t\t\t%d\t\t%%g\t\t\t%d to %g", sizeof(double), -(1 << PowerOfdouble), ((1 << PowerOfdouble) - 1));

}