/*
3.  Make two files as "Source.c" and "First.c"
	a.	In "Source.c", Write a basic program (as mentioned in class).
	b.	Declare an Integer with "extern" keyword e.g: "extern int a;"
	c.  print this varibale in main() function.

	d.  In "First.c" write "int a=7;" (Any number will do)

	e. Compile the code and observe the output.
*/
//------------------------------------------------------------------------------------
//code
//------------------------------------------------------------------------------------
#include <stdio.h>

extern int SourceVariable;
int main(void)
{
	//extern int SourceVariable;
	printf("Value of variable in main : %d", SourceVariable);
}
