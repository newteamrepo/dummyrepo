/*
12.	Write a program to find factorial of a given number
	Note : use "While Loop" and "For Loop".
*/
//-------------------------------------------------------------------------------------------------
//code
//-------------------------------------------------------------------------------------------------
#include <stdio.h>

int main(void)
{
	int num, temp, place = 1;
	int fact[120];
	int carry = 0;
	int n = 1;
	fact[0] = 1;

	printf("Enter the number[0-100]\n");
	scanf("%d", &num);

	if (num == 0)
	{
		printf("Factorial of  0 is 1\n");
		return (0);
	}
	else
	{	
		printf("Calculating Factorial using for loop.\n");
		for (n = 1; n <= num; n++)			// to multiply n number(for 5 , it will be 1,2,3,4,5)
		{
			for (int j = 0; j < place; j++)
			{
				temp = (fact[j] * n) + carry;
				fact[j] = temp % 10;
				carry = temp / 10;
			}
			while (carry != 0)
			{
				fact[place] = carry % 10;
				carry = carry / 10;
				place++;
			}
		}
	}
	//print the ans (u need to reverse the array to get the result)
	int end = place - 1;
	int start = 0;
	while (start < end)
	{
		temp = fact[end];
		fact[end] = fact[start];
		fact[start] = temp;
		start++;
		end--;
	}
	for (int i = 0; i < place; i++)
	{
		printf("%d", fact[i]);
	}
}