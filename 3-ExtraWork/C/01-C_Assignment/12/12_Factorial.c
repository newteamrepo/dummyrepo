/*
12.	Write a program to find factorial of a given number
	Note : use "While Loop" and "For Loop".
*/
//-------------------------------------------------------------------------------------------------
//code
//-------------------------------------------------------------------------------------------------
#include <stdio.h>

int main(void)
{
	int num, fact = 1;
	
	printf("Enter the number[0-100]\n");
	scanf("%d", &num);

	if (num == 0)
	{
		printf("Factorial of  0 is 1\n");
		return (0);
	}
	else
	{
		printf("Calculating Factorial using while loop.\n")
		printf("Factorial of %d: ", num);
		while (num != 1)
		{
			fact = fact * num;
			num = num - 1;
		}
		printf("%d\n", fact);
	}

	

	
	
}