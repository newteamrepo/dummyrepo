/*9. program to accept a character 'c'  and display category of the input type
		ALPHABET        :       c is a letter (65 to 90 or 97 to 122)
		UPPERCASE       :       c is UPPERCASE Letter (65 to 90)
		LOWERCASE       :       c is LOWERCASE letter (97 to 122)
		DIGIT           :       c is DIGIT (48 to 57)
		SPACE           :       c is space(32),tab (9), carriage return(13), new line(10)
		OTHERS          :       Not Listed Above
*/

//-------------------------------------------------------------------------------------------------
//code
//-------------------------------------------------------------------------------------------------
#include <stdio.h>

int main(void)
{
	char c;
	printf("Enter Your Favourite Character.\n");
	scanf("%c", &c);
	printf("%d\n", c);
	if (((c >= 65) && (c <= 90)) || ((c >= 97) && (c <= 122)))
	{
		printf("Character is letter\n");
		if ((c >= 65) && (c <= 90))
			printf(" Character is UPPERCASE letter.\n");
		else if ((c >= 97) && (c <= 122))
			printf("Character is LOWERCASE letter.\n");
	}
	else if ((c >= 48) && (c <= 57))
		printf("Character id DIGIT.\n");
	else if (c == 32)
		printf("Characer is SPACE.\n");
	else if (c == 9)
		printf("Characer is TAB.\n");
	else if (c == 13)
		printf("Characer is CARRIAGE RETURN.\n");
	else if (c == 10)
		printf("Characer is NEWLINE.\n");
	else
		printf("Character is other than alphabet, digit and space.\n");
}
