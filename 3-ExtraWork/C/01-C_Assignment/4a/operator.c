/* Write a Program to Accept two numbers (say a and b, you may take any names) and
		a. Calculate it's Sum
		b. Calculate it's Substraction
		c. Calculate it's product
		d. Calculate it's Division (display error while dividing by zero)
		e. Calculate it's Modulus (display error while dividing by zero)
		Test the program using different integral data types signed/unsigned char/int/long.
		Start the result with small values, later on test it with larger values.
		Observe it's results.*/

		//--------------------------------------------------------------------------------------------
		//Code
		//--------------------------------------------------------------------------------------------
#include <stdio.h>

void Operations(void *, void *);

int main(void)
{
	int iNum1, iNum2, option;
	printf("Press 1 for arithmethic operation on signed integers\n");
	printf("Press 2 for arithmethic operation on unsigned integers\n");
	printf("Press 3 for arithmethic operation on char\n");
	printf("Press 4 for arithmethic operation on signed long\n");
	printf("Press 5 for arithmethic operation on unsigned long\n");
	scanf("%d", &option);
	while (option)
	{
		switch (option)
		{
		case 1:
		{
			int Num1 = 0, Num2 = 0;
			printf("Enter two signed integers\n");
			scanf("%d\n", &Num1);
			scanf("%d", &Num2);
			printf("Addition: %d\n", (Num1 + Num2));
			printf("Substraction: %d\n", (Num1 - Num2));
			printf("Multiplication: %d\n", (Num1*Num2));
			if (Num2 == 0)
				printf("Division not possible, denominator is 0.\n");
			else
				printf("Division(Quotient): %d\n", (Num1 / Num2));
			if (Num2 == 0)
				printf("Modulus not possible, denominator is 0.\n");
			else
				printf("Modulus(remainder): %d\n", (Num1%Num2));
			printf("press option to continue or 0 to quit./n");
			scanf("%d", &option);
		}
		break;
		case 2:
		{
			unsigned int Num1 = 0;
			unsigned int Num2 = 0;
			printf("Enter two unsigned integers\n");
			scanf("%u\n", &Num1);
			scanf("%u", &Num2);
			printf("Addition: %u\n", (Num1 + Num2));
			printf("Substraction: %d\n", (Num1 - Num2));
			printf("Multiplication: %d\n", (Num1*Num2));
			if (Num2 == 0)
				printf("Division not possible, denominator is 0.\n");
			else
				printf("Division(Quotient): %d\n", (Num1 / Num2));
			if (Num2 == 0)
				printf("Modulus not possible, denominator is 0.\n");
			else
				printf("Modulus(remainder): %d\n", (Num1%Num2));
			printf("press option to continue or 0 to quit./n");
			scanf("%d", &option);
		}
		break;
		case 3:
		{
			 char Char1;
			 char Char2;
			printf("Enter 1st char:");
			Char1 = getch();
			printf("\nEnter 2nd char:");
			Char2 = getch();
			printf("\nChar 1 = %c and Char 2 =%c\n ", Char1, Char2);
			
			printf("\nAddition: %c\n", (Char1 + Char2));
			printf("Substraction: %c\n", (Char1 - Char2));
			printf("Multiplication: %c\n", (Char1 * Char2));
			if (Char2 == 0)
				printf("Division not possible, denominator is 0.\n");
			else
				printf("Division(Quotient): %c\n", (Char1 / Char2));
			if (Char2 == 0)
				printf("Modulus not possible, denominator is 0.\n");
			else
				printf("Modulus(remainder): %c\n", (Char1 % Char2));
			printf("press option to continue or 0 to quit.\n");
			scanf("%d", &option);
		}
		break;
		case 4:
		{
			signed long a, b;
			printf("Enter value of a :");
			scanf("%ld",&a);
			printf("\n");
			printf("Enter value of b :");
			scanf("%ld",&b);
			printf("\n");
			//printf("char1 : %c and char2 : %c\n",Char1, Char2);
			printf("Addition: %ld\n", (a+ b));
			printf("Substraction: %ld\n", (a - b));
			printf("Multiplication: %ld\n", (a * b));
			if (b == 0)
				printf("Division not possible, denominator is 0.\n");
			else
				printf("Division(Quotient): %ld\n", (a / b));
			if (b == 0)
				printf("Modulus not possible, denominator is 0.\n");
			else
				printf("Modulus(remainder): %ld\n", (a % b));
			printf("press option to continue or 0 to quit.\n");
			scanf("%d", &option);
		}
		break;
		case 5:
			unsigned long a, b;
			printf("Enter value of a :");
			scanf("%ld",&a);
			printf("\n");
			printf("Enter value of b :");
			scanf("%ld",&b);
			printf("\n");
			printf("a : %ld and b : %ld\n",a, b);
			printf("Addition: %ld\n", (a+ b));
			printf("Substraction: %ld\n", (a - b));
			printf("Multiplication: %ld\n", (a * b));
			if (b == 0)
				printf("Division not possible, denominator is 0.\n");
			else
				printf("Division(Quotient): %ld\n", (a / b));
			if (b == 0)
				printf("Modulus not possible, denominator is 0.\n");
			else
				printf("Modulus(remainder): %ld\n", (a % b));
			printf("press option to continue or 0 to quit.\n");
			scanf("%d", &option);
			break;
		case 0:
			return(0);
			break;
		}//switch
	}
}

