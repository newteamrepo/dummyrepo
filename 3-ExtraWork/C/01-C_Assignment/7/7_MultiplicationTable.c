/*7. Program to Accept a number and print table of that number.
   Make sure to Indent the table properly (Do not use Spaces in printf rather use escape sequences).
*/

//-----------------------------------------------------------------------------------------------------
//code
//-----------------------------------------------------------------------------------------------------
#include<stdio.h>

int main(void)
{
	int num, i;
	printf("Enter the number.\n");
	scanf("%d", &num);
	for (i = 1; i <= 10; i++)
	{
		printf("%d\t*\t%d\t=%d\n", num, i, (i*num));
	}
}
