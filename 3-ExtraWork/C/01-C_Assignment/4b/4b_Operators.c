/*4b. Write a Program to Accept two numbers (say a and b, you may take any names) and print the following results
		a. (a&b)
		b. (a&&b)
		c. (a|b)
		d. (a||b)
		e. !a , !b
		f. ~a,~b
		g. (a^b)
		h. a<<2,b<<1
		i. a>>2,b>>1
*/
//---------------------------------------------------------------------------------------------------------------
//code
//---------------------------------------------------------------------------------------------------------------
#include <stdio.h>

int main(void)
{
	int a, b;
	printf("Enter two numbers\n");
	scanf("%d", &a);
	scanf("%d", &b);
	printf("value of (a&b) : %d\n", (a & b));
	printf("Value of (a&&b) : %d\n", (a&&b));
	printf("Value of (a|b) : %d\n", (a | b));
	printf("value of (a || b) : %d\n", (a || b));
	printf("Value of  !a : %d\n", !a);
	printf("Value of  !b : %d\n", !b);
	printf("Value of  ~a : %d\n", ~a);
	printf("Value of  ~b : %d\n", ~b);
	printf("Value of (a^b) : %d\n", (a^b));
	printf("Value of a<<2 : %d, b << 1 : %d\n", (a << 2), (b << 1));
	printf("Value of a>>2 : %d, b >> 1 : %d\n", (a >> 2), (b >> 1));
}
