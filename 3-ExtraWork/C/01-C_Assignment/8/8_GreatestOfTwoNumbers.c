/*8. Program to find  greatst of two numbers.
		i. With if-else
   ii.  Ternary Operator
*/

//---------------------------------------------------------------------------------------
//code
//---------------------------------------------------------------------------------------

#include <stdio.h>

int main(void)
{
	int num1, num2;
	printf("enter two numbers.\n");
	scanf("%d", &num1);
	scanf("%d", &num2);
	printf("using if-else\n");
	if (num1 > num2)
		printf("num1 is greatest\n");
	else
		printf("num2 is greatest\n");
	printf("using ternary operator\n");
	(num1 > num2) ? printf("num1 is greatest\n") : printf("num2 is greatest\n");
}
